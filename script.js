db.users.insertMany([
	{"First Name": "Diane", "Last Name": "Murphy", "Email": "dmurphy@mail.com", "Is Admin": false, "Is Active": true},
	{"First Name": "Mary", "Last Name": "Patterson", "Email": "mpatterson@mail.com", "Is Admin": false, "Is Active": true},
	{"First Name": "Jeff", "Last Name": "Firrelli", "Email": "jfirrelli@mail.com", "Is Admin": false, "Is Active": true},
	{"First Name": "Gerard", "Last Name": "Bondur", "Email": "gbondur@mail.com", "Is Admin": false, "Is Active": true},
	{"First Name": "Pamela", "Last Name": "Castillo", "Email": "pcastillo@mail.com", "Is Admin": true, "Is Active": false},
	{"First Name": "George", "Last Name": "Vanauf", "Email": "gvanauf@mail.com", "Is Admin": true, "Is Active": true},

])

db.courses.insertMany([
	{"Name": "Professional Development", "Price": 10000},
	{"Name": "Business Processing", "Price": 13000}
])



db.users.find({$or:[{"First Name":{$regex:"A",$options:"i"}},{"Last Name":{$regex:"A",$options:"i"}}]},
					{"Email": 0, "Is Admin": 0, "Is Active": 0, "_id": 0})


db.users.find({$and:[{"Is Admin": true},{"Is Active": true}]})

db.courses.find({$and:[{"Name":{$regex:"u",$options:"i"}},{"Price":{$gte:13000}}]})